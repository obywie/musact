
$(document).ready(function() {
    // are we running in native app or in a browser?
    window.isphone = false;
    if(document.URL.indexOf("http://") === -1 
        && document.URL.indexOf("https://") === -1) {
        window.isphone = true;
    }

    if( window.isphone ) {
        document.addEventListener("deviceready", onDeviceReady, false);
    } else {
        onDeviceReady();
    }
});

function onDeviceReady() {    
        setTimeout(function () {
            window.location.href = "login.html"; //will redirect to your blog page (an ex: blog.html)
         }, 2000); //will call the function after 2 secs.
}